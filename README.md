This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

# 1. Snehal Portfolio Website

My mini portfolio website to know about my skills interests and contact details to get in touch with me.

---

## 2. Setup

These are the instructions to run the project:

1. Open the terminal and navigate to the folder where this readme file is located.
1. Install the project dependencies by typing `npm install` on the terminal.
1. Start the project by typing `npm start` on the terminal.

---

## 3. Usage instructions

The landing page portfolio website that is only one page without internal links, that highlights the projects that I have done so far.

---

## 4. Dependencies

Beside React JS and it's own dependencies, this project uses:

1. Recoil for global state management of the shopping list state.
1. Firebase for hosting.

---

## 5. Project organization

### 5.1 Requirement gathering

The document submited by the fictional company as the coding test. It is highlighted using color to differenciate the functional, non-functional requrirements among other key elements.

[Google Docs link](https://docs.google.com/document/d/1PBvO70U862IVOX6DyWULWDUEefeEAbTIeYERpMTUoSU/edit)

---

### 5.2 Project spreadsheet

A spreadsheet with the information related to the organization of the project.

[Google Spreadsheets link](https://docs.google.com/spreadsheets/d/1A-AmoYlJVMR8PtgAMrxDVAP7d4HwLpoAZUJp1dr2IVs/edit)

---

### 5.3 Design mockup

A low detail mockup to visualize how the app will look.

The focus is on colors, fonts, branding and the positions of each element on the screen.

[Figma link](https://www.figma.com/file/mSUM4p5N8ImgOUI0AZoorJ/Portfolio?node-id=2%3A516)

---

### 5.4 Component tree

The component tree allows to visualize the overall hierarchy of the project.

The first diagram covers the overal application layout:
[View latest version](https://whimsical.com/portfolio-5FpfFzZ6Z2CbxJFXf2Qngh)

## 6. Atribbutions

This project uses the following external art assets:

- Portfolio logo: design on photoshop.
- Icons: Font awesome 5.