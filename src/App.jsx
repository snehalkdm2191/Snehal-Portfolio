import React, { useEffect,useState } from "react";
import { useRecoilState } from "recoil";
import { listState } from "./state/listState";
import ProjectListFile from "../src/data/ProjectList.json";
import "./css/App.css";
import NavBar from "./components/NavBar";
import MobNavBar from "./components/MobNavBar";
import Hero from "./sections/Hero";
import Projects from "./sections/Projects";
import Modal from "./components/Modal";
import Technologies from "./sections/Technologies";
import About from "./sections/About";
import Contact from "./sections/Contact";

export default function App() {
  // Global state
  const [list, setList] = useRecoilState(listState);
  // Local state
  const [modal, setModal] = useState(null);

  // Methods
  function loadData(setList) {
    const parsedData = ProjectListFile ?? [];

    setList(parsedData);
  }

  useEffect(() => loadData(setList));

  const isMobile = window.innerWidth <= 800;
  return (
    <div>
      {isMobile ? <MobNavBar /> : <NavBar />}
      <Hero />
      <About />
      {list.length !== 0 ? <Projects setModal={setModal} /> : null}
      <Technologies />
      <Contact />

      <Modal state={[modal, setModal]} />
    </div>
  );
}
