import React from "react";
import PortfolioFooter from "../components/PortfolioFooter";

export default function Contact() {
  return (
    <div>
    <div id="section-contact" className="row">
      <div className="col-sm-6">
        <h2>Contact me</h2>
        <p>
          If you're interested in reaching out to me, I'd love<br/> to connect and work with you!
        </p>
      </div>
      <div className="col-sm-6 contact-details">
        <a href="mailto:snehalkdm2191@gmail.com"><i class="fas fa-envelope-open-text"/>
        <span>snehalkdm2191@gmail.com</span></a>
        <br /><br/>
        <i class="fas fa-mobile-alt"/><span>+46-735831881</span>
        <br /><br/>
        <i class="fas fa-map-marked-alt"/><span>Stockholm,Sweden</span>
      </div>
    </div>
    <PortfolioFooter/>
    </div>
  );
}