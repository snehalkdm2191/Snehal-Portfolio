import React from "react";
import { useRecoilState } from "recoil";
import { listState } from "../state/listState";
import ProjectCard from "../components/ProjectCard";
import ProjectModal from "../components/ProjectModal";

function Projects({ setModal }) {
  const [list] = useRecoilState(listState);
  // Component
  const ProjectRows = list.map((projectList, index) => (
    <ProjectCard
      key={index}
      projectList={projectList}
      onClick={() => setProject(projectList, projectList.complete)}
    />
  ));

  function setProject(projectList, isComplete) {
    if (isComplete) setModal(<ProjectModal projectList={projectList} />);
  }

  return (
    <div id="section-project" className="row">
      <div className="col-md-4">
        <h2>Projects</h2>
        <p className="project-para">
          Here are the projects I will be making during the Frontend course at
          Novare Potential.
        </p>
      </div>
      <div className="col-md-8">
        <div className="project-list">{ProjectRows}</div>
      </div>
    </div>
  );
}
export default Projects;
