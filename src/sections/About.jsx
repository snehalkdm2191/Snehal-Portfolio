import React from "react";
import profileImg from "../img/snehal-pic.png";
//import getResume from "../data/Snehal-Kadam.pdf";
import getResume from "../data/SnehalKadam-CV.pdf";
import AboutData from "../data/AboutData.json";

function About() {
  return (
    <div id="section-about" className="row">
      <div className="col-md-7 about-text">
        <h2>About me</h2>
        {AboutData.map((aboutData) => (
          <>
            <p>
              {aboutData.aboutMe[0]}
              <br />
              {aboutData.aboutMe[1]}
            </p>
            <p>{aboutData.aboutMe[2]}</p>
          </>
        ))}
        <a className="btn-resume" href={getResume} target="_blank">
          <span class="btn-gradient">
            <i class="far fa-list-alt"></i>
          </span>
          <span class="btn-text">Get Resume</span>
        </a>
      </div>
      <div className="col-md-1"></div>
      <div className="col-md-4">
        <img className="about-img" src={profileImg} alt="Snehal pic" />
      </div>
    </div>
  );
}
export default About;
