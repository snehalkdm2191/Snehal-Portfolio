import React from "react";
import techList from "../data/TechnologiesList.json";

export default function Technologies() {
  return (
    <div id="section-tech" className="row">
      <div className="col-md-4">
        <h2>Technologies</h2>
        <p>
          Here is the techology that I already know and also starting to learn.
        </p>
      </div>
      <div className="col-md-8 tech-card-div">
    
          <div className="tech-list">
            {techList.map((tech) => (
              <div className="tech-icon">
                <img
                  src={require(`../img/${tech.icon}`).default}
                  alt={tech.name}
                />
                <span>{tech.name}</span>
              </div>
            ))}
          </div>
        
      </div>
    </div>
  );
}
