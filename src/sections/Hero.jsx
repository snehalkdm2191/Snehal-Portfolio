import React from "react";
import Typewriter from "typewriter-effect";

function Hero() {
  const heroContentString = ["Full stack developer", "Frontend developer","Enthusiastic developer"];
  return (
      <div id="section-hero">
        <div className="hero-img"></div>
        <div className="hero-content">
          <h3>Hello, I'M <span>Snehal</span></h3>
          <Typewriter
            options={{
              strings: heroContentString,
              autoStart: true,
              loop: true,
            }}
          />
          <p>Knack of building applications with front and <br/> back end operations.</p>
        </div>
      </div>
  );
}

export default Hero;
