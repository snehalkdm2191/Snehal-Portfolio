import React from "react";
import techList from "../data/TechnologiesList.json";

function Technologies() {
  return (
    <div id="section-tech" className="row">
      <div className="col-md-4">
        <h2>Technologies</h2>
        <p>
          Here is the techology that I already know and also starting to learn.
        </p>
      </div>
      <div className="col-md-8 tech-card-div">
        <div className="container">
          <div className="row">
            {techList.map((tech) => (
              <div className="col-sm-3">
                <div className="card text-center tech-card">
                  <img
                    class="card-img-top"
                    src={require(`../img/${tech.icon}`).default}
                    alt={tech.name}
                  />
                  <div class="card-body">
                    <span>{tech.name}</span>
                  </div>
                </div>
              </div>
            ))}
          </div>
        </div>
      </div>
    </div>
  );
}
export default Technologies;
