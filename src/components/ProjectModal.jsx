import React from "react";
import Pills from "./Pills";

export default function ProjectModal({ projectList }) {
  const { title, img, description, technologies, hostLink, gitLink } =
    projectList;
  return (
      <div className="row">
        <div id="modal" className="col-md-7">
          <img
            className="modal-img"
            src={require(`../img/${img}`).default}
            alt={title}
          />
        </div>
        <div className="col-md-5">
          <span className="modal-title">{title}</span>
          <p className="modal-description">{description}</p>
          <Pills tech={technologies} />
          <br />
          <a className="btn-modal-link btn btn-dark" href={hostLink} target="_blank">
            <i class="far fa-window-maximize"></i> View Website/App
          </a>
          <br />
          <a className="btn-modal-link btn btn-outline-dark" href={gitLink} target="_blank">
            <i className="fab fa-gitlab"></i> Git repository
          </a>
        </div>
      </div>
  );
}
