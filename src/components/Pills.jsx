import React from "react";

function Pills({ tech }) {
  return (
    <ul className="pills-section">
      {tech.map((tech) => (
          <li className="pills-title">{tech}</li>
      ))}
    </ul>
  );
}
export default Pills;
