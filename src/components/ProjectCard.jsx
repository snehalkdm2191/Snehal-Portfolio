import React from "react";

export default function ProjectCard({ projectList, onClick }) {
  const { img, title, complete } = projectList;
  const ComingSoonProjects = complete ? null : (
    <div class="close-card-text">
      <p>Coming soon</p>
    </div>
  );

  return (
    <button className="btn-project-card" onClick={onClick}>
      <div className="project-card">
        {ComingSoonProjects}
        <img
          className={complete ? null : "close-card"}
          src={require(`../img/${img}`).default}
          alt={title}
        />
        <span>{title}</span>
      </div>
    </button>
  );
}
