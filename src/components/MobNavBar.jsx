import React, { useState,useEffect } from "react";
import logo from "../img/portfolio-logo.png";

function MobNavBar() {
  const [nav, setNav] = useState(false);

  function changeBackground() {
    if (window.scrollY >= 50) {
      setNav(true);
    } else {
      setNav(false);
    }
  }

  useEffect(() => window.addEventListener("scroll", changeBackground));

  return (
    <nav className={nav ? "nav-mob active" : "nav-mob"}>
      <ul className="menu">
        <li>
          <a href="#section-about">About</a>
        </li>
        <li>
          <a href="#section-project">Projects</a>
        </li>
        <li>
          <a href="#section-hero" className="logo">
          <img src={logo} alt="logo" />
          </a>
        </li>
        <li>
          <a href="#section-tech">Tech</a>
        </li>
        <li>
          <a href="#section-contact">Contact</a>
        </li>
      </ul>
    </nav>
  );
}

export default MobNavBar;
