import React from "react";
import facebookImg from "../img/facebook.png";
import linkedinImg from "../img/linkedin.png";
import twitterImg from "../img/twitter.png";

function PortfolioFooter() {
  return (
    <div id="PortfolioFooter">
      <p>
        <a className="footer-links" href="https://www.linkedin.com/in/snehal-kadam-alsundkar-45601016b/">
        <img src={linkedinImg} alt=""/>
        </a>
        <a className="footer-links" href="https://github.com/snehalkdm2191/">
        <img src={twitterImg} alt=""/>
        </a>
        <a className="footer-links" href="https://gitlab.com/snehalkdm2191">
        <img src={facebookImg} alt=""/>
        </a>
        <br />
        <span className="copyright-text">© made with <i class="fas fa-heart"></i> Passion & a Dream by Snehal Kadam</span>
      </p>
    </div>
  );
}
export default PortfolioFooter;
